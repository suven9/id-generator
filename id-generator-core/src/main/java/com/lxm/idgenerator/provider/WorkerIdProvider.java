package com.lxm.idgenerator.provider;

public interface WorkerIdProvider {
    /**
     * 获取工作机器id
     * @return
     */
    long getWorkerId();
}
